<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->get('/', function () use ($router) {
    return "API NINJA. <br> Service Build With <b>" . $router->app->version() . "</b><br> API Version V.1.1.0";
});

$router->group(['prefix' => 'api/'], function ($app) {
    $app->post('verif', ['uses' =>'hana\ClientController@verify_webhook']);
    $app->post('header', ['uses' =>'hana\HeaderController@header']);

    $app->post('on_delivery', ['uses' =>'hana\OnDeliveryController@OnDelivery']);
    $app->post('successful_delivery', ['uses' =>'hana\SuccessfulDeliveryController@SuccessfulDelivery']);
    $app->post('cancel', ['uses' =>'hana\CancelController@Cancel']);
    $app->post('return_to_sender_trigger', ['uses' =>'hana\ReturnToSenderTriggerController@ReturnToSenderTrigger']);
    $app->post('return_to_sender', ['uses' =>'hana\ReturnToSenderController@ReturnToSender']);
    $app->post('pending_pickup', ['uses' =>'hana\PendingPickup@PendingPickup']);
});

$router->group(['prefix' => 'api/main'], function ($app) {
    $app->post('insert_shipper_master', ['uses' =>'main\ShipperMasterController@InsertShipperMaster']);
    $app->post('get_shipper_master', ['uses' =>'main\ShipperMasterController@GetShipperMaster']);

    $app->post('insert_driver', ['uses' =>'main\DriverController@InsertDriver']);
    $app->post('insert_batch_driver', ['uses' =>'main\DriverController@InsertBatchDriver']);
    $app->post('get_driver', ['uses' =>'main\DriverController@GetDriver']);

    $app->post('get_webhook', ['uses' =>'main\WebhookLogController@GetWebhook']);
});



