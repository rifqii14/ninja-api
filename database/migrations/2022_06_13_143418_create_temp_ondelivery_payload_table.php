<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempOndeliveryPayloadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_payloads', function (Blueprint $table) {
            $table->id();
            $table->string('ext_id')->nullable();
            $table->string('shipper_id')->nullable();
            $table->string('status')->nullable();
            $table->string('shipper_ref_no')->nullable();
            $table->string('tracking_ref_no')->nullable();
            $table->string('shipper_order_ref_no')->nullable();
            $table->string('timestamp')->nullable();
            $table->string('previous_status')->nullable();
            $table->string('tracking_id')->nullable();
            $table->string('comments')->nullable();
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('identity_number')->nullable();
            $table->string('contact')->nullable();
            $table->string('uri')->nullable();
            $table->string('left_in_safe_place')->nullable();
            $table->string('payload_status')->nullable();
            $table->string('payload_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_payloads');
    }
}
