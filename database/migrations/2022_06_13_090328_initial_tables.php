<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->string('shipper_id');
            $table->string('order_id')->nullable();
            $table->string('tracking_id')->nullable();
            $table->string('driver_name')->nullable();
            $table->timestamps();
        });

        Schema::create('log_webhooks', function (Blueprint $table) {
            $table->id();
            $table->string('shipper')->nullable();
            $table->dateTime('date')->nullable();
            $table->text('response_code_webhook')->nullable();
            $table->text('response_body_webhook')->nullable();
            $table->text('response_result_webhook')->nullable();
            $table->string('order_id')->nullable();
            $table->string('tracking_id')->nullable();
            $table->string('event_type')->nullable();
            $table->string('url_webhook')->nullable();
            $table->text('response_code_client')->nullable();
            $table->text('response_body_client')->nullable();
            $table->text('response_result_client')->nullable();
            $table->text('url_client')->nullable();
            $table->timestamps();
        });

        Schema::create('shippers', function (Blueprint $table) {
            $table->id();
            $table->string('ext_shipper_id')->nullable();
            $table->string('name')->nullable();
            $table->string('client_id')->nullable();
            $table->string('client_secret')->nullable();
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
        Schema::dropIfExists('log_webhooks');
        Schema::dropIfExists('shippers');
        Schema::dropIfExists('users');
    }
}
