<?php

namespace App\Jobs;

use DB;
use App\TempPayload;
use App\Libraries\HanaLibrary;
use Carbon\Carbon;

class RetrySuccessfulDeliveryJob extends Job
{
    /**
     * The podcast instance.
     *
     * @var \App\TempPayload
     */
    protected $tempPayload;

    protected $hana_library;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TempPayload $tempPayload)
    {
        $this->tempPayload = $tempPayload;
        $this->hana_library  = new HanaLibrary();
    }

    public $tries = 3;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $param = $this->tempPayload;

        $driver = DB::table('driver')->where('tracking_id', $param->tracking_id)->first();

        if($driver){
            $payload = array(
                "shipper_id" => (int) $param->shipper_id,
                "status" =>  $param->status,
                "shipper_ref_no" =>  $param->shipper_ref_no,
                "tracking_ref_no" =>  $param->tracking_ref_no,
                "shipper_order_ref_no" =>  $param->shipper_order_ref_no,
                "timestamp" => $param->timestamp,
                "id" =>  $param->ext_id,
                "previous_status" =>  $param->previous_status,
                "tracking_id" =>  $param->tracking_id,
                "type" => $param->type,
                "name" => $param->name,
                "identity_number" => $param->identity_number,
                "contact" => $param->contact,
                "uri" => $param->uri,
                "left_in_safe_place" => $param->left_in_safe_place == 0 ? false : true,
                "driver" => $driver->driver_name
            );

            $execute_payload = $this->hana_library->successful_delivery($payload);

            $response = [
                "status" => "OK",
                "response_code" => 200,
                "time" => Carbon::Now()
            ];

            $data = array(
                "date" => Carbon::now(),
                'shipper'=> $param->shipper_id,
                "event_type" => 'successful_delivery',
                "order_id" => $param->shipper_order_ref_no,
                "tracking_id" => $param->tracking_id,
                "response_code_webhook" => $response['response_code'],
                "response_body_webhook" => json_encode($payload),
                "response_result_webhook" => json_encode($response),
                'url' => gethostname().'/successful_delivery',
                "response_code_client" => $execute_payload['result']->code,
                "response_body_client" => $execute_payload['body'],
                "response_result_client" => json_encode($execute_payload['result']),
                "url_client" => $execute_payload['url']
            );

            if ($execute_payload['result']->code === 1000) {

                $checkLog = DB::table('log_webhook')->where(['tracking_id' => $param->tracking_id, 'response_code_client' => null, 'event_type' => 'successful_delivery']);

                if ($checkLog->count() !== 0) {
                   $checkLog->update($data);
                } else {
                    DB::table('log_webhook')->insert($data);
                }

                $this->tempPayload->delete();
            } else {
                $this->fail();
            }

            if ($this->attempts() > 2) {

                $checkLog = DB::table('log_webhook')->where(['tracking_id' => $param->tracking_id, 'response_code_client' => null, 'event_type' => 'successful_delivery']);

                if ($checkLog->count() !== 0) {
                   $checkLog->update($data);
                } else {
                    DB::table('log_webhook')->insert($data);
                }

                $this->tempPayload->delete();
            }
        }
    }
}
