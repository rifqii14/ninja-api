<?php

namespace App\Imports;

use App\Driver;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class DriverImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        if (Driver::where('tracking_id', $row['tracking_id'])->count() !== 0) {
            return null;
        }

        return new Driver([
            'shipper_id' => $row['shipper_id'],
            'order_id' => $row['order_id'],
            'tracking_id' => $row['tracking_id'],
            'driver_name' => $row['driver_name']
        ]);
    }

    // public function batchSize(): int
    // {
    //     return 200;
    // }

    // public function chunkSize(): int
    // {
    //     return 300;
    // }
}
