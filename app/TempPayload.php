<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempPayload extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ext_id',
        'shipper_id',
        'status',
        'shipper_ref_no',
        'tracking_ref_no',
        'shipper_order_ref_no',
        'timestamp',
        'previous_status',
        'tracking_id',
        'comments',
        'payload_status',
        'payload_type',
        'identity_number',
        'type',
        'name',
        'uri',
        'left_in_safe_place',
        'contact',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
