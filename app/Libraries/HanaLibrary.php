<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use App\Libraries\ResponseLibrary;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;

class HanaLibrary {

	public function __construct() {
		$this->headers = "";
        $this->url = env('DEV_URL');
	}

    function getHeader(){

        $this->generate_header();
        return $this->headers;
    }

    function on_delivery($payload) {
		$dataparam      = array(
            "devRefNo"=> $payload['shipper_order_ref_no'],
            "devVendorOrdNo" => $payload['tracking_id'],
            "devSts" => '0002',
            "devBranchNm" => $payload['comments'],
            "devDriverNm" => $payload['driver'],
            "remark" => $payload['status']
        );

		$this->action = "On Delivery";
        $this->req_url = "/v1/carddev/65101";
		$this->endpoint = $this->url.$this->req_url;

        $this->generate_header();

		$result   = $this->execute($this->endpoint, json_encode($dataparam));

        return array(
            'body' => json_encode($dataparam),
            'result' => json_decode($result),
            'url' =>  $this->req_url,
        );
    }

    function successful_delivery($payload) {
		$dataparam      = array(
            "devRefNo"=> $payload['shipper_order_ref_no'],
            "devVendorOrdNo" => $payload['tracking_id'],
            "devSts" => '0009',
            "devBranchNm" => 'ID-BANDUNG-Gunung Halu',
            "devDriverNm" => $payload['driver'],
            "remark" => $payload['name']
        );
		$this->action = "Successful Delivery";
        $this->req_url = "/v1/carddev/65101";
		$this->endpoint = $this->url.$this->req_url;
        $this->generate_header($this->req_url);
		$result   = $this->execute($this->endpoint, json_encode($dataparam));

        return array(
            'body' => json_encode($dataparam),
            'result' => json_decode($result),
            'url' =>  $this->req_url,
        );
    }

    function cancel($payload) {
		$dataparam      = array(
            "devRefNo"=> $payload['shipper_order_ref_no'],
            "devVendorOrdNo" => $payload['tracking_id'],
            "devSts" => '0019',
            "devBranchNm" => '-',
            "devDriverNm" => '-',
            "remark" => $payload['comments']
        );

		$this->action = "Cancel";
        $this->req_url = "/v1/carddev/65101";
		$this->endpoint = $this->url.$this->req_url;
        $this->generate_header($this->req_url);
		$result   = $this->execute($this->endpoint, json_encode($dataparam));

        return array(
            'body' => json_encode($dataparam),
            'result' => json_decode($result)
        );
    }

    function return_to_sender_trigger($payload) {
		$dataparam      = array(
            "devRefNo"=> $payload['shipper_order_ref_no'],
            "devVendorOrdNo" => $payload['tracking_id'],
            "devSts" => '0099',
            "devBranchNm" => 'branchname',
            "devDriverNm" => $payload['driver'],
            "remark" => $payload['comments']
        );

		$this->action = "Return to Sender Trigger";
        $this->req_url = "/v1/carddev/65101";
		$this->endpoint = $this->url.$this->req_url;
        $this->generate_header($this->req_url);
		$result   = $this->execute($this->endpoint, json_encode($dataparam));

        return array(
            'body' => json_encode($dataparam),
            'result' => json_decode($result),
            'url' =>  $this->req_url,
        );
    }

    function buildSignature($client_secret,$payload){
        $key = base64_decode($client_secret);
        $msg = utf8_decode($payload);

        $generateSign = hash_hmac('sha256',$msg,$key,true);
        // echo 'FINAL SIGN= '. base64_encode($generateSign);
        return base64_encode($generateSign);
    }

    function generate_header(){
        $client_id = 'dbcard-deliv4';
        $client_secret = 'kCxMbaIHzUz9g3k0WuDLH7a159J0c+ag8oVfYiMrRk0=';
        $timestamp = round(microtime(true) * 1000);
        $madstring = 'ABC'.rand(100,999);
        //starting lbcommon
        $data_lbcommon = array(
            'globalId' => date('Ymd').'CRDDEV-d'.date('YmdHis').'00',
            'langCd' => 'EN',
            'sysCd' => 'PT',
            'instCd' => '1234',
            'ipv6Adr' => '172.16.158.86',
            'chnlType' => 'LNA',
            'mediaCd' => '12',
            'deviceId' => 'IT134',
            'mobileOsCd' => 'A8.1',
            'reqUrlAdr' => '/v1/sign/01101',
            'reqTs' => $timestamp,
            'svcId' => '01101'
        );
        $lbcommon = base64_encode(json_encode($data_lbcommon));
        //end lbcommon

        $payload = 'LINE'.$client_id.';'.$timestamp.';'.$madstring.'BANK';

        $sig = $this->buildSignature($client_secret,$payload);

        $this->headers = array(
            'Content-Type: application/json',
            'lbcommon: '.$lbcommon,
            'Authorization: '.'Credential='.$client_id.'/'.$timestamp.'/'.$madstring.';Signature='.$sig
        );
    }

    public function execute($url, $data) {
		Log::info("");
		Log::info("<<<<<<<<<< Start Call Hana Service >>>>>>>>>>");
		$this->datas = $data;
		Log::info('[Action] : '.$this->action);
		Log::info('[Request] : '.$this->datas );
		$start = microtime(true);
    	$curl = curl_init();
    	curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
    	curl_setopt($curl, CURLOPT_POST, 1);
    	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    	curl_setopt($curl, CURLOPT_URL, $url);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
		$server_output = curl_exec($curl);

        // dd(curl_getinfo($curl, CURLINFO_HEADER_OUT ));

		$end = microtime(true);
		$this->response_time = $end - $start;
		$this->http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        //cek tidak ada balasan
        if (empty($server_output)) {
            curl_close($curl);
			$this->response = [
				"code" => '99',
				"message" => 'Gagal Connect ke service Hana',
				];
			$this->log_api_out();
			return json_encode($this->response);

        }

        //check http code response
        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200)
        {
            curl_close($curl);
			$this->response = [
				"code" => '99',
				"message" => curl_getinfo($curl, CURLINFO_HTTP_CODE).' - Gagal Connect ke service Hana',
				];
			$this->log_api_out();
			return json_encode($this->response);
        }

        //return jika http 200
        if(curl_getinfo($curl, CURLINFO_HTTP_CODE) == 200)
        {
			curl_close($curl);
			$this->response = json_decode($server_output,true);
			$this->log_api_out();
			return $server_output;
        }

	}

	// write log
	function log_api_out(){
		// write log
		Log::info('[Response] : '.json_encode($this->response));
		Log::info("<<<<<<<<<< End Call Hana Service >>>>>>>>>>");
	}

}
