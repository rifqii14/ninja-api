<?php


namespace App\Libraries;

class Constant {
  /** 
   * General
   */
   	const RC_SUCCESS = "00";
	const DESC_SUCCESS = "sukses";

  	const RC_PARAM_NOT_VALID = "PNV";
	const DESC_PARAM_NOT_VALID = "Parameter tidak valid";
	
  	const RC_REQUEST_NOT_VALID = "RNV";
    const DESC_REQUEST_NOT_VALID = "Format request tidak valid";

    const RC_DATA_NOT_FOUND = "DNF";
	const DESC_DATA_NOT_FOUND = "Data tidak ditemukan";

    const RC_DB_ERROR = "DE";
	const DESC_DB_ERROR = "Terjadi kesalahan pada saat akses database";
	
	const RC_FORMAT_RESPONSE_NOT_VALID = "FNV";
	const DESC_FORMAT_RESPONSE_NOT_VALID = "Terjadi kesalahan pada format response";

	CONST REQUEST_DATA = 'request_data';
}