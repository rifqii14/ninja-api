<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogWebhook extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shipper',
        'date',
        'response_code_webhook',
        'response_body_webhook',
        'response_result_webhook',
        'order_id',
        'tracking_id',
        'event_type',
        'url',
        'response_code_client',
        'response_body_client',
        'response_result_client',
        'url_client'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
