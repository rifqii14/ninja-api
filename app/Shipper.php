<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipper extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ext_shipper_id',
        'name',
        'client_id',
        'client_secret',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
