<?php

namespace App\Console\Commands;

use App\Jobs\RetryOnDeliveryJob;
use App\Jobs\RetryReturnToSenderTriggerJob;
use App\Jobs\RetryReturnToSenderJob;
use App\Jobs\RetrySuccessfulDeliveryJob;
use App\TempPayload;
use Illuminate\Console\Command;

class RetryJobCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retry:job {type : Type of request payload will be retry [on_delivery, succesful_delivery, return_to_sender_trigger, return_to_sender]}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retry Job Request';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $type = $this->argument('type');

        $tempPayloads = TempPayload::where('payload_type', $type)->get();

        $bar = $this->output->createProgressBar(count($tempPayloads));

        $bar->start();

        foreach ($tempPayloads as $payload)
        {
            if ($type === 'on_delivery') {
                dispatch(new RetryOnDeliveryJob($payload));
            }

            if ($type === 'succesful_delivery') {
                dispatch(new RetrySuccessfulDeliveryJob($payload));
            }

            if ($type === 'return_to_sender_trigger') {
                dispatch(new RetryReturnToSenderTriggerJob($payload));
            }

            if ($type === 'return_to_sender') {
                dispatch(new RetryReturnToSenderJob($payload));
            }

            $bar->advance();
        }

        $bar->finish();

        return 0;
    }
}
