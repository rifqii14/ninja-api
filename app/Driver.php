<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{

    public $timestamps = false;
    protected $table = 'driver';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shipper_id', 'order_id', 'tracking_id', 'driver_name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
