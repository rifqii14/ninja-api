<?php

namespace App\Http\Controllers\hana;

use App\Http\Controllers\Controller;
use App\Libraries\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ResponseLibrary;
use App\Libraries\RequestLibrary;
use DB;

class ClientController extends Controller{

    public function __construct() {
		$this->response 	  = new ResponseLibrary();
		$this->request_param  = new RequestLibrary();
    }

    
    function verify_webhook(){

          $client_secret =  '9c7b822f1209409694010b0b20b0b042a8c4d58ff0b44839b57b51b60ab3c42f';
          $data = file_get_contents('php://input');

          $calculated_hmac = base64_encode(hash_hmac('sha256', $data , $client_secret, true));

          $hmac_header = 'Ru6nS2bSEPRaRAq+GM0cMMvVxyFz8sVkKjSI6rD1jgY=';
          return ($calculated_hmac);
    }
    
}