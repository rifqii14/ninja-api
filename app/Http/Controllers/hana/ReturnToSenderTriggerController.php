<?php

namespace App\Http\Controllers\hana;

use App\Http\Controllers\Controller;
use App\Libraries\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ResponseLibrary;
use App\Libraries\RequestLibrary;
use App\Libraries\HanaLibrary;
use App\TempPayload;
use Carbon\Carbon;
use DB;

class ReturnToSenderTriggerController extends Controller{

    public function __construct() {
		$this->response 	  = new ResponseLibrary();
		$this->request_param  = new RequestLibrary();
		$this->hana_library  = new HanaLibrary();
    }

    public function ReturnToSenderTrigger(Request $request){
        $rules = [
            "shipper_id" => "nullable",
            "status" =>  "nullable",
            "shipper_ref_no" =>  "nullable",
            "tracking_ref_no" =>  "nullable",
            "shipper_order_ref_no" =>  "nullable",
            "timestamp" => "nullable",
            "id" =>  "nullable",
            "previous_status" =>  "nullable",
            "tracking_id" =>  "nullable",
            "comments" =>  "nullable"
		];

        $validator = Validator::make($request->all(), $rules);
		if ($validator->fails()) {
			return $this->response->format_response("return_to_sender_trigger",$validator->errors()->first());
		}

		$param = $this->request_param->get_param($request->all());

        $driver = \DB::table('driver')->where('tracking_id',$param->tracking_id)->first();

        if(!$driver){
            $temp = TempPayload::where(['tracking_id' => $param->tracking_id, 'payload_type' => 'return_to_sender_trigger']);

            if ($temp->count() === 0 ) {
                $tempPayload = new TempPayload();
            } else {
                $tempPayload = $temp->first();
            }

            $tempPayload->shipper_id = $param->shipper_id;
            $tempPayload->status = $param->status;
            $tempPayload->shipper_ref_no = $param->shipper_ref_no;
            $tempPayload->tracking_ref_no = $param->tracking_ref_no;
            $tempPayload->shipper_order_ref_no = $param->shipper_order_ref_no;
            $tempPayload->timestamp = $param->timestamp;
            $tempPayload->ext_id = $param->id;
            $tempPayload->previous_status = $param->previous_status;
            $tempPayload->tracking_id = $param->tracking_id;
            $tempPayload->comments = $param->comments;
            $tempPayload->payload_type = 'return_to_sender_trigger';
            $tempPayload->save();

            if(http_response_code() != 200){
                $response = [
                    "status" => "Failed",
                    "response_code" => http_response_code(),
                    "time" => Carbon::Now()
                ];
            }else{
                $response = [
                    "status" => "OK",
                    "response_code" => http_response_code(),
                    "time" => Carbon::Now()
                ];
            }

            $data = array(
                "date" => Carbon::now(),
                'shipper'=> $param->shipper_id,
                "event_type" => 'return_to_sender_trigger',
                "order_id" => $param->shipper_order_ref_no,
                "tracking_id" => $param->tracking_id,
                "response_code_webhook" => $response['response_code'],
                "response_body_webhook" => json_encode($request->all()),
                "response_result_webhook" => json_encode($response),
                'url' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
            );

            \DB::table('log_webhook')->insert($data);

            return $this->response->format_response("return_to_sender_trigger", 'middleware success');
        }

        if(http_response_code() != 200){
            $response = [
                "status" => "Failed",
                "response_code" => http_response_code(),
                "time" => Carbon::Now()
            ];
        }else{
            $response = [
                "status" => "OK",
                "response_code" => http_response_code(),
                "time" => Carbon::Now()
            ];
        }

        $payload = array(
            "shipper_id" => $param->shipper_id,
            "status" =>  $param->status,
            "shipper_ref_no" =>  $param->shipper_ref_no,
            "tracking_ref_no" >  $param->tracking_ref_no,
            "shipper_order_ref_no" =>  $param->shipper_order_ref_no,
            "timestamp" => $param->timestamp,
            "id" =>  $param->id,
            "previous_status" =>  $param->previous_status,
            "tracking_id" =>  $param->tracking_id,
            "comments" =>  $param->comments,
            "driver" => $driver->driver_name
        );

        // $execute_payload = $this->hana_library->return_to_sender_trigger($payload);

        $data = array(
            "date" => Carbon::now(),
            'shipper'=> $param->shipper_id,
            "event_type" => 'return_to_sender_trigger',
            "order_id" => $param->shipper_order_ref_no,
            "tracking_id" => $param->tracking_id,
            "response_code_webhook" => $response['response_code'],
            "response_body_webhook" => json_encode($request->all()),
            "response_result_webhook" => json_encode($response),
            'url' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]",
            "response_code_client" => '',
            "response_body_client" => '',
            "response_result_client" => json_encode('')
            // "response_code_client" => $execute_payload['result']->code,
            // "response_body_client" => $execute_payload['body'],
            // "response_result_client" => json_encode($execute_payload['result'])
        );

        $insert = \DB::table('log_webhook')->insert($data);

        return $this->response->format_response("return_to_sender_trigger", $data);
    }


}
