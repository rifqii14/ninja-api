<?php

namespace App\Http\Controllers\hana;

use App\Http\Controllers\Controller;
use App\Libraries\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ResponseLibrary;
use App\Libraries\RequestLibrary;
use App\Libraries\HanaLibrary;
use Carbon\Carbon;
use DB;

class PendingPickup extends Controller{

    public function __construct() {
		$this->response 	  = new ResponseLibrary();
		$this->request_param  = new RequestLibrary();
		$this->hana_library  = new HanaLibrary();
    }

    public function PendingPickup(Request $request){
        $rules = [   
            "shipper_id" => "nullable",
            "status" =>  "nullable",
            "shipper_ref_no" =>  "nullable",
            "tracking_ref_no" =>  "nullable",
            "shipper_order_ref_no" =>  "nullable",
            "timestamp" => "nullable",
            "id" =>  "nullable",
            "tracking_id" =>  "nullable"
		];

        $validator = Validator::make($request->all(), $rules);
		$param = $this->request_param->get_param($request->all()); 
        
        if ($validator->fails()) {
            return $this->response->format_response_(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "pending pickup");
        }

        if(http_response_code() != 200){
            $response = [
                "status" => "Failed",
                "response_code" => http_response_code(),
                "time" => Carbon::Now()
            ];
        }else{
            $response = [
                "status" => "OK",
                "response_code" => http_response_code(),
                "time" => Carbon::Now()
            ];
        }

        $data = array(
            "date" => Carbon::now(),
            'shipper'=> $param->shipper_order_ref_no,
            "event_type" => 'Pending Pickup',
            "tracking_number" => $param->shipper_order_ref_no,
            "response_code" => $response['response_code'],
            "response_body" => json_encode($request->all()),
            "response_result" => json_encode($response),
            'url' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
        );

        $insert = \DB::table('log_webhook')->insert($data);

        return $this->response->format_response_(Constant::RC_SUCCESS,Constant::DESC_SUCCESS, "pending pickup", $response);

    }

}