<?php

namespace App\Http\Controllers\main;

use App\Http\Controllers\Controller;
use App\Libraries\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ResponseLibrary;
use App\Libraries\RequestLibrary;
use DB;

class WebhookLogController extends Controller{

    public function __construct() {
		$this->response 	  = new ResponseLibrary();
		$this->request_param  = new RequestLibrary();
    }

    public function GetWebhook(Request $request){
        $rules = [   
            "id_log" => "nullable",
            "date" =>  "nullable",
            "shipper" =>  "nullable",
            "tracking_number" =>  "nullable"
		];

        $validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
            return $this->response->format_response_(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "get webhook log");
		}

		$param = $this->request_param->get_param($request->all()); 
        
        if($param->id_log != ''){
            $get = \DB::table('log_webhook')->select('*')->where('id_log',$param->id_log)->get();
        }else{
            $get = \DB::table('log_webhook')->select('*')->orderBy('date','desc')->get();
        }

        if($get->isEmpty()){
            Log::info('Data Not Found'); 
            return $this->response->format_response_(Constant::RC_DB_ERROR, "Data Not Found'", "get webhook log");
        }

        return $this->response->format_response_(Constant::RC_SUCCESS,Constant::DESC_SUCCESS, "get webhook log", $get);
    }

}