<?php

namespace App\Http\Controllers\main;

use App\Http\Controllers\Controller;
use App\Imports\DriverImport;
use App\Libraries\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ResponseLibrary;
use App\Libraries\RequestLibrary;
use DB;

class DriverController extends Controller{

    public function __construct() {
		$this->response 	  = new ResponseLibrary();
		$this->request_param  = new RequestLibrary();
    }

    public function GetDriver(Request $request){
        $rules = [
            "shipper_id" => "nullable",
            "order_id" =>  "nullable",
            "tracking_id" =>  "nullable",
            "driver_name" =>  "nullable"
		];

        $validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
            return $this->response->format_response_(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "get driver");
		}

		$param = $this->request_param->get_param($request->all());


        $get = \DB::table('driver')->select('*')->get();

        if($get->isEmpty()){
            Log::info('Data Not Found');
            return $this->response->format_response_(Constant::RC_DB_ERROR, "Data Not Found'", "get driver");
        }

        return $this->response->format_response_(Constant::RC_SUCCESS,Constant::DESC_SUCCESS, "get driver", $get);
    }

    public function InsertDriver(Request $request){
        $rules = [
            "shipper_id" => "required",
            "order_id" =>  "nullable",
            "tracking_id" =>  "required",
            "driver_name" =>  "required"
		];

        $validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
            return $this->response->format_response_(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "insert driver");
		}

		$param = $this->request_param->get_param($request->all());


        $data = [
            'shipper_id' => $param->shipper_id,
            'order_id' => $param->order_id,
            'tracking_id' => $param->tracking_id,
            'driver_name' => $param->driver_name
        ];

        $insert = \DB::table('driver')->insert($data);

        if(!$insert){
            Log::info('Failed insert driver');
            return $this->response->format_response_(Constant::RC_DB_ERROR, "Failed insert driver'", "insert driver");
        }

        return $this->response->format_response_(Constant::RC_SUCCESS,Constant::DESC_SUCCESS, "insert driver", $insert);
    }

    public function InsertBatchDriver(Request $request){
        $rules = [
            'file' => 'required|file|mimes:csv,xls,xlsx'
        ];

        $validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
            return $this->response->format_response_(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "insert driver");
		}

        Excel::import(new DriverImport, request()->file('file'));

        return $this->response->format_response_(Constant::RC_SUCCESS, Constant::DESC_SUCCESS, "insert driver");
    }

}
