<?php

namespace App\Http\Controllers\main;

use App\Http\Controllers\Controller;
use App\Libraries\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Libraries\ResponseLibrary;
use App\Libraries\RequestLibrary;
use DB;

class ShipperMasterController extends Controller{

    public function __construct() {
		$this->response 	  = new ResponseLibrary();
		$this->request_param  = new RequestLibrary();
    }

    public function GetShipperMaster(Request $request){
        $rules = [   
            "shipper_id" => "nullable",
            "shipper_name" =>  "nullable",
            "client_id" =>  "nullable",
            "client_secret" =>  "nullable"
		];

        $validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
            return $this->response->format_response_(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "get shipper master");
		}

		$param = $this->request_param->get_param($request->all()); 


        $get = \DB::table('shipper_master')->select('*')->get();

        if($get->isEmpty()){
            Log::info('Data Not Found'); 
            return $this->response->format_response_(Constant::RC_DB_ERROR, "Data Not Found'", "get shipper master");
        }

        return $this->response->format_response_(Constant::RC_SUCCESS,Constant::DESC_SUCCESS, "get shipper master", $get);
    }

    public function InsertShipperMaster(Request $request){
        $rules = [   
            "shipper_id" => "required",
            "shipper_name" =>  "required",
            "client_id" =>  "required",
            "client_secret" =>  "required"
		];

        $validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
            return $this->response->format_response_(Constant::RC_PARAM_NOT_VALID, $validator->errors()->first(), "insert shipper master");
		}

		$param = $this->request_param->get_param($request->all()); 


        $data = [
            'shipper_id' => $param->shipper_id,
            'shipper_name' => $param->shipper_name,
            'client_id' => $param->client_id,
            'client_secret' => $param->client_secret
        ];

        $insert = \DB::table('shipper_master')->insert($data);

        if(!$insert){
            Log::info('Failed insert shipper master'); 
            return $this->response->format_response_(Constant::RC_DB_ERROR, "Failed insert shipper master'", "insert shipper master");
        }

        return $this->response->format_response_(Constant::RC_SUCCESS,Constant::DESC_SUCCESS, "insert shipper master", $insert);
    }

    
}